﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ProjectPaint_GK
{
    public partial class Form1 : Form
    {
        Graphics gp;
        Color myColor;
        Pen myPen;
        int x = -1;
        int y = -1;
        bool bLine = false;
        bool bRect = false;
        bool bFillRect = false;
         bool bFillEcllipse=false;
        bool bEcllipse = false;
        bool bVe = false;
        private Point p1;
        private Point p2;
        bool isPress = false;
        private Bitmap bm;
        private Color currColor;
        private int currSize;

        public enum ShapModel
        {
           
        }
        public Form1()
        {
            InitializeComponent();

            currColor = Color.Black;
            myPen = new Pen(currColor);
            bm = new Bitmap(this.Width, this.Height);
            gp = Graphics.FromImage(bm);
            //UserPaint điều khiển khả năng vẽ không phụ thuộc hệ điều hành tránh rung
            this.SetStyle(ControlStyles.UserPaint, true);
            //AllPaintingInWmPaint ngats tị nhắn hệ diều hành
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            InitCBBox();
        }


        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            isPress = true;
            x = e.X;
            y = e.Y;
            p1 = new Point(e.Location.X, e.Location.Y);
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (isPress)
            {
                p2 = new Point(e.Location.X, e.Location.Y);
                this.Refresh();
                if (this.bVe==true && x != -1 && y != -1)
                {
                    //Cập nhật lại bút vẽ
                    myPen = new Pen(currColor, (float)currSize);
                    gp.DrawLine(myPen, new Point(x, y), e.Location);
                    x = e.X;
                    y = e.Y;
                    this.BackgroundImage = (Bitmap)bm.Clone();
                }
            }

        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            this.isPress = false;
            if (this.bVe)
            {
                x = -1;
                y = -1;
                this.bVe = false;
            }
           
           else if (bLine)
            {
                Pen pen = new Pen(currColor, (float)currSize);
                gp.DrawLine(pen, p1, p2);
                this.bLine = false;
            }
           else if (this.bEcllipse)
            {
                Pen pen = new Pen(currColor, (float)currSize);
                gp.DrawEllipse(pen, p1.X, p1.Y, p2.X - p1.X, p2.Y - p1.Y);
                this.bEcllipse = false;
            }
            else if (this.bRect)
            {

                Pen pen = new Pen(currColor, (float)currSize);
                gp.DrawRectangle(pen, p1.X, p1.Y, p2.X - p1.X, p2.Y - p1.Y);
                this.bRect = false;
            }
            else if (this.bFillRect)
            {
                SolidBrush sb = new SolidBrush(currColor);
                gp.FillRectangle(sb, p1.X, p1.Y, p2.X - p1.X, p2.Y - p1.Y);
                this.bFillRect = false;
            }
            else if (this.bFillEcllipse)
            {
                SolidBrush sb = new SolidBrush(currColor);
                gp.FillEllipse(sb, p1.X, p1.Y, p2.X - p1.X, p2.Y - p1.Y);
                this.bFillEcllipse = false;
            }
            this.BackgroundImage = (Bitmap)bm.Clone();

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            if (isPress)
            {
                if (bLine)
                {
                    Pen pen = new Pen(currColor, (float)currSize);
                    e.Graphics.DrawLine(pen, p1, p2);
                   
                }
                if (this.bEcllipse)
                {
                    Pen pen = new Pen(currColor, (float)currSize);
                    e.Graphics.DrawEllipse(pen, p1.X, p1.Y, p2.X - p1.X, p2.Y - p1.Y);
                }
                else if (this.bRect)
                {

                    Pen pen = new Pen(currColor, (float)currSize);
                    e.Graphics.DrawRectangle(pen, p1.X, p1.Y, p2.X - p1.X, p2.Y - p1.Y);
                }
                else if (this.bFillRect)
                {
                    SolidBrush sb = new SolidBrush(currColor);
                    e.Graphics.FillRectangle(sb, p1.X, p1.Y, p2.X - p1.X, p2.Y - p1.Y);
                }
                else if (this.bFillEcllipse)
                {
                    SolidBrush sb = new SolidBrush(currColor);
                    e.Graphics.FillEllipse(sb, p1.X, p1.Y, p2.X - p1.X, p2.Y - p1.Y);
                }

            }
        }

        private void btnLine_Click(object sender, EventArgs e)
        {
            this.bLine = true;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.bEcllipse = true;
        }

        private void btnMau_Click(object sender, EventArgs e)
        {
            ColorDialog cld = new ColorDialog();
            if (cld.ShowDialog() == DialogResult.OK)
            {
                currColor = cld.Color;
            }
        }
        private void InitCBBox()
        {
            for(int i = 1; i < 10; i++)
            {
                cbxSize.Items.Add(i);
            }
            cbxSize.SelectedIndex = 0;
        }
        private void cbxSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            currSize = int.Parse(cbxSize.Text);

        }

        private void btnDrawRectangle_Click(object sender, EventArgs e)
        {
            this.bRect = true;
        }

        private void btnFillRectangle_Click(object sender, EventArgs e)
        {
            this.bFillRect = true;
        }

        private void btnFillEcllipse_Click(object sender, EventArgs e)
        {
            this.bFillEcllipse = true;
        }

        private void btnVe_Click(object sender, EventArgs e)
        {
            this.bVe = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult traloi;
            traloi = MessageBox.Show("Bạn có muốn thoát", "Trả lời", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (traloi == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DialogResult traloi;
            traloi = MessageBox.Show("Bạn có muốn thoát", "Trả lời", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (traloi == DialogResult.OK)
            {
                Application.Exit();
            }
           
        }

        OpenFileDialog open;
        SaveFileDialog save;
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            save = new SaveFileDialog();
            save.Filter = "|*.txt";
            save.RestoreDirectory = true;
            if (save.ShowDialog() == DialogResult.OK)
            {
                StreamWriter write = new StreamWriter(save.FileName);
                write.Close();
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            open = new OpenFileDialog();
            open.Filter = "|*.txt";
            if (open.ShowDialog() == DialogResult.OK)
            {
                StreamReader read = new StreamReader(open.FileName);
                read.Close();

            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.None;
            this.Left = 0;
            this.Top = 0;
            this.Bounds = Screen.PrimaryScreen.Bounds;

        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 newForm = new Form1();
            newForm.Show();
            this.Dispose(false);
        }
    }
  
}
